#include <iostream>
#include <fstream>
#include <vector>
#include <regex>

#include "Produs.h"
#include "Categorie.h"

int main()
{
	std::vector<Product> products;

	uint16_t id;
	std::string productName;
	float price;
	uint16_t tva;
	std::string expiringDateOrCategoryName;

	for (std::ifstream f("product.prodb"); !f.eof();)
	{
		f >> id >> productName >> price >> tva >> expiringDateOrCategoryName;

		std::regex DateRegex(R"(\d\d\d\d-\d\d-\d\d)");

		if (std::regex_match(expiringDateOrCategoryName, DateRegex) == true)
		{
			///Product produs(id, productName, price, tva, expiringDateOrCategoryName );
			///products.push_back(produs);
			products.emplace_back(id, productName, price, tva, expiringDateOrCategoryName);
		}
		else 
		{
			///Product produs(id, productName, price, tva, stringInCategorie(expiringDateOrCategoryName));
			///products.push_back(produs);
			products.emplace_back(id, productName, price, tva, stringInCategorie(expiringDateOrCategoryName));
		}
	}
	for (auto it : products) 
	{
		if (it.getTva() == 19) 
		{
			std::cout << it;
		}
	}
}