#pragma once
#include<cstdint>
#include <string>
enum class Category : uint8_t
{
	PersonalHygiene,
	Clothing,
	SmallAppliences
};
Category stringInCategorie(const std::string& str);
std::string categorieInString(Category category);