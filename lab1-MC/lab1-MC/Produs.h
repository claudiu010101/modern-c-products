#pragma once
#include <string>
#include <cstdint>
#include "Categorie.h";
#include <iostream>

class Product
{
	uint16_t id;
	std::string productName;
	float price;
	uint16_t tva;
	std::string expiringDate;
	Category categoryName;
public:
	Product(uint16_t id, const std::string& productname, float price, uint16_t tva, const std::string& expiringdate);
	Product(uint16_t id, const std::string& productname, float price, uint16_t tva, Category category);
	uint16_t getTva() const;
	friend std::ostream& operator<<(std::ostream& f, const Product& produs);
	float getPrice() const;
};

