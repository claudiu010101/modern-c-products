#include "Categorie.h"
Category stringInCategorie(const std::string& str) 
{
	if (str == "PersonalHygiene") 
	{
		return Category::PersonalHygiene;
	}
	else if (str == "Clothing") 
	{
		return Category::Clothing;
	}
	else if (str == "SmallAppliences") 
	{
		return Category::SmallAppliences;
	}
	else 
	{
		throw "Eroare, categoria nu exista!\n";
	}

}
std::string categorieInString(Category categorie) 
{
	switch (categorie) 
	{
	case Category::PersonalHygiene:
		return "PersonalHygiene";
	case Category::Clothing:
		return "Clothing";
	case Category::SmallAppliences:
		return "SmallAppliences";
	}
}