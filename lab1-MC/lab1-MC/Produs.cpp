#include "Produs.h"

Product::Product(uint16_t id, const std::string& productname, float price, uint16_t tva, const std::string& expiringdate)
	:id{ id }, productName{ productname }, price{ price }, tva{ tva }, expiringDate{ expiringdate }
{

}

Product::Product(uint16_t id, const std::string& productname, float price, uint16_t tva, Category category)
	: id{ id }, productName{ productname }, price{ price }, tva{ tva }, categoryName{ category }
{

}

uint16_t Product::getTva() const 
{
	return tva;
}
float Product::getPrice() const 
{
	return price + price * tva / 100;
}

std::ostream& operator<<(std::ostream& f, const Product& produs) 
{
	f << produs.id << ' ' << produs.productName << ' ' << produs.getPrice() << ' ' << produs.tva << ' ';
	if (produs.tva == 9) 
	{
		f << produs.expiringDate;
	}
	else 
	{
		f << categorieInString(produs.categoryName);
	}
	f << std::endl;
	return f;
}